// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyAMAfybeYy1c80kHSCT9onn9CSXHPXHAQM",
    authDomain: "pushnotification-979ab.firebaseapp.com",
    projectId: "pushnotification-979ab",
    storageBucket: "pushnotification-979ab.appspot.com",
    messagingSenderId: "221379957428",
    appId: "1:221379957428:web:a617128400bdb07feabde1",
    measurementId: "G-9KPC7M8Z6D"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
