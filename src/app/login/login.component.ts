import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  title = 'push-notification';
  message;
  form: any;
  constructor(private authservice:AuthService,private fb:FormBuilder,private router:Router) { }
ngOnInit() {
  this.form = this.fb.group({
    sEmail: ['',Validators.required],
    sPassword : ['', Validators.required],
    rememberme: [false]
  });

  
  // this.messagingService.requestPermission()
  // this.messagingService.receiveMessage()
  // this.message = this.messagingService.currentMessage
 }


  submit(){
    console.log(this.form.value);
    
    this.authservice.login(this.form.value).subscribe(
      user => {
     
        if(!this.form.value.rememberme){
          localStorage.removeItem('login');
          sessionStorage.setItem('login',  JSON.stringify(user.data.sToken));
         
        }else{
          localStorage.setItem('login', JSON.stringify(user.data.sToken));
          localStorage.setItem('rememberme', JSON.stringify("logged"));

        }
        this.router.navigate(['home']);
      },
      err => {
        
         console.log(err);
      }
    );
  }

}