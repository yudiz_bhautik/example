import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  private baseUrl = 'https://ba5kwska3d.execute-api.ap-south-1.amazonaws.com/stag'

  constructor(private http:HttpClient,private router:Router) { }

  login(data): any {
    return this.http.post<any>(`${this.baseUrl}/auth/login`, data).pipe(
      map(user => {
        console.log(user);
        
          return user;

      })
    );
  }
}
