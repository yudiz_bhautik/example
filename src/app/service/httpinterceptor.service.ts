import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { map, catchError, mergeMap, concatMap, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class HttpinterceptorService implements HttpInterceptor {
  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

   
      let token = JSON.parse(sessionStorage.getItem('login'))
      let localStorageToken = JSON.parse(localStorage.getItem('login'));
      if(token !== null){
         req = req.clone({
           setHeaders: { Authorization : token}
         });
      }
      if(localStorageToken !== null){
       req = req.clone({
         setHeaders: { Authorization : localStorageToken}
       });
      }

      return next.handle(req).pipe(
        tap(event => {
          if (event instanceof HttpResponse) {
          }
        }, error => {
         
            console.log(error);
        })
      )

      
  }


  

}
